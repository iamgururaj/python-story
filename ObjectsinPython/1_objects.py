# Identifiers objects in python - The most important of all Python commands is an assignment statement, such as

temperature = 99.8

# This command establishes temperature as an identifier (also known as a name), and then associates it with the
# object expressed on the right-hand side of the equal sign, in this case a floating-point object with
# value 98.8

"""
Note:
    - Identifiers in Python are case-sensitive
    - Identifiers can be composed of almost any combination of letters, numerals, and underscore
    characters (or more general Unicode characters)
    - The primary restrictions are that an identifier cannot begin with a numeral (thus 9lives is an illegal name)
    - 33 specially reserved words that cannot be used as identifiers - Find them as follows
        -- False
        -- as
        -- continue
        -- None
        -- assert
        -- def
        -- True
        -- break
        -- del
        -- and
        -- class
        -- elif
        -- for
        -- import
        -- nonlocal
        -- raise
        -- with
        -- else
        -- from
        -- in
        -- not
        -- return
        -- yield
        -- except
        -- global
        -- is
        -- or
        -- try
        -- finally
        -- if
        -- lambda
        -- pass
        -- while
    
    - Unlike Java and C++, Python is a dynamically typed language, as there is no advance declaration associating 
    an identifier with a particular data type. An iden- tifier can be associated with any type of object, and it can
    later be reassigned to another object of the same (or different) type. Although an identifier has no declared
    type, the object to which it refers has a definite type. In our first example, the characters 98.6 are recognized
    as a floating-point literal, and thus the identifier temperature is associated with an instance of the float class
    having that value
    
"""
